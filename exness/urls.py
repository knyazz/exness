from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/api/checker/$', 'base.views.checker', name='api_checker'),
    url(r'^admin/checker/form/$', 'base.views.checker_form', 
        name='checker_form'),
    url(r'^api/auth/',include('rest_framework.urls',
        namespace='rest_framework')),
]
