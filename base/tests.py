# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.test.client import Client
from django.db import transaction

from django_nose.testcases import FastFixtureTestCase

from .models import UserIPv4Address


class BaseTest(FastFixtureTestCase):
    fixtures = ('exness/fixtures/init.json',)
    username = 'knyazz@inbox.ru'

    def setUp(self):
        self.test_client = Client()
        transaction.set_autocommit(True)

    def logining(self):
        _pswd = '1'
        params = { 
                    'password': _pswd,
                    'username': self.username,
                }
        response=self.test_client.post( reverse('rest_framework:login'),
                                        params)
        self.assertEqual(response.status_code, 302)

    def get_user(self):
        user = User.objects.filter(username=self.username).last()
        self.assertIsNotNone(user)
        return user


class BaseAPITest(BaseTest):
    def base_create_ip_address(self):
        '''
            test models
        '''
        user = self.get_user()
        _init= dict(user=user, ip_address='1.2.3.9')
        instance = UserIPv4Address.objects.create(**_init)
        self.assertIsNotNone(instance.pk)
        UserIPv4Address.objects.filter(**_init).delete()
        self.get_user()
        instance = UserIPv4Address.objects.filter(**_init).last()
        self.assertIsNone(instance)
        try:
            UserIPv4Address.objects.create(user=user, ip_address='1.2.3.5')
        except IntegrityError:
            pass

    def base_simple_tests(self):
        '''
            test base functionality
        '''
        # auth
        self.logining()
        # check orm
        self.base_create_ip_address()
        # check form
        response = self.test_client.get(reverse('checker_form'))
        self.assertEqual(response.status_code, 200)
        # check API
        res = self.test_client.get(reverse('api_checker')+'?user1=1&user2=4')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.get('result'), True)
        res = self.test_client.get(reverse('api_checker')+'?user1=1&user2=2')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.get('result'), False)
        res = self.test_client.get(reverse('api_checker')+'?user1=1&user2=3')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.get('result'), False)
        # test throttle
        res = self.test_client.get(reverse('api_checker')+'?user1=1&user2=2')
        self.assertEqual(res.status_code, 429)