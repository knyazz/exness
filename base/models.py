from django.contrib.auth.models import User
from django.db import models


class UserIPv4Address(models.Model):
    user = models.ForeignKey(User)
    ip_address = models.GenericIPAddressField(protocol='IPv4')
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'ip_address')
        index_together = unique_together