#coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from .models import UserIPv4Address

class BaseListAdmin(admin.ModelAdmin):
    def get_list_filter(self, request, obj=None):  
        return self.get_fields(request, obj)
        
    def get_list_display(self, request, obj=None):
        return ['id',]+self.get_fields(request, obj)

admin.site.register(UserIPv4Address, BaseListAdmin)