#coding: utf-8
from __future__ import unicode_literals
import re

from django.views.generic import FormView
from django.core.urlresolvers import reverse
from django.db import connections

from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from .forms import CheckUserIPsForm
from .models import UserIPv4Address


with connections['default'].cursor() as cur:
    value_name = 'ip_address'
    table_name = UserIPv4Address._meta.db_table
    raw_select = '''
        SELECT set_masklen({}::cidr,24)
        as mask FROM {} WHERE user_id = ANY($1)
        GROUP BY {} HAVING COUNT(*) > 1
    '''.format(value_name, table_name, value_name)
    raw_execute = '''
        CREATE OR REPLACE FUNCTION scan(int[]) RETURNS integer  AS $$
        DECLARE
          x RECORD;
          y RECORD;
        BEGIN
          FOR x IN {}
          LOOP
            FOR y IN {}
            LOOP
              IF x.mask <> y.mask THEN
            RETURN 1;
              END IF;
            END LOOP;
          END LOOP;
          RETURN 0;
        END;
        $$ LANGUAGE plpgsql;
    '''.format(raw_select, raw_select)
    cur.execute(raw_execute)


class CheckUserIPs(APIView):
    b''' API проверки пользователей '''
    permission_classes = (IsAdminUser,)
    allowed_methods = ('GET',)
    throttle_scope = 'default' # 3 requests in minute
    python_only = 'no' # yes or no or mixed, yes is not recommended


    def get(self, request, format=None):
        b'''основной метод, GET запрос,
            возвращает http ответ с json
        '''
        if request.query_params:
                usrs = set((request.query_params.get('user1'),
                            request.query_params.get('user2')))
                if usrs and len(usrs) == 2:
                    if self.python_only == 'yes':
                        res = self.check_ips_python(usrs)
                    else:
                        usrs = self.check_sql_injection(usrs)
                        if self.python_only == 'no':
                            res = self.check_ips(usrs)
                        else:
                            res = self.check_ips_mixed(usrs)
                    res = dict(result = res if res is not None else False)
                else:
                    res = dict(result='munch, munch... strange food.')
                return Response(data=res,status=200)
        return Response(status=404)

    def check_sql_injection(self, users=None):
        # проверяем на sql injection id ли пользователей передаются
        if users:
            if all(e.isdigit() for e in users):
                return ','.join(users)

    def check_ips(self, users=None):
        b'''
            проверяем в БД для данных пользователей
            наличие разных сетей с общими ip, используя
            таблицы с промежуточными значениями и plpsql функцию,
            возвращающую 1, если взаимосвязаны, иначе 0
            Функиця инициализирована выше
        '''
        if users:
            raw_execute = '''
                SELECT scan(ARRAY[{}]);
            '''.format(users)
            with connections['default'].cursor() as cur:
                cur.execute(raw_execute)
                res = cur.fetchall()
                return res[0][0] if res and res[0] else 0

    def check_ips_mixed(self, users=None):
        b'''
            проверяем в БД для данных пользователей
            наличие разных сетей с общими ip, используя
            таблицы с промежуточными значениями
        '''
        if users:
            value_name = 'ip_address'
            table_name = UserIPv4Address._meta.db_table
            raw_select = '''
                SELECT set_masklen({}::cidr,24)
                as mask FROM {} WHERE user_id IN ({})
                GROUP BY {} HAVING COUNT(*) > 1;
            '''.format(value_name, table_name, users, value_name)
            with connections['default'].cursor() as cur:
                cur.execute(raw_select)
                res = cur.fetchall()
                return self.check_nets_mixed(res)

    def check_nets_mixed(self, nets=None, val=None):
        b''' проверяем есть ли различные сети '''
        if nets:
            if val:
                val1=val
            else:
                val1 = nets.pop()
            if nets:
                val2 = nets.pop()
                if val1 == val2 :
                    self.check_nets_mixed(nets, val2)
                else:
                    return 1

    def check_ips_python(self, users=None):
        b'''
            проверяем в БД для данных пользователей
            наличие разных сетей с общими ip, используя
            python sets
        '''
        if users:
            _ips1 = UserIPv4Address.objects.filter(user_id=users.pop()
                                        ).values_list('ip_address', flat=True)
            _ips2 = UserIPv4Address.objects.filter(user_id=users.pop()
                                        ).values_list('ip_address', flat=True)
            res = set(_ips1) & set(_ips2)
            if len(res) > 1:
                return self.check_nets_python(res)

    def check_nets_python(self, ips=None, val=None):
        b''' проверяем есть ли ip в различных сетях в set с ip_address '''
        if ips:
            if val:
                val1=val
            else:
                val1 = ips.pop()
            val2 = ips.pop()
            if self._get_net(val1) == self._get_net(val2):
                self.check_nets_python(ips, val2)
            else:
                return 1

    def _get_net(self, ip_address):
        b''' /24 сеть для ip-адреса ''' 
        res = re.findall(r'\d+.\d+.\d+', ip_address)
        return res and res[0]
checker = CheckUserIPs.as_view()


class CheckUserIPsFormView(FormView):
    b''' Форма проверки взаимосвязи пользователeй по ip адресу '''
    form_class = CheckUserIPsForm
    template_name = 'admin/checkuserips_form.html'

    def get_success_url(self):
        return reverse('checker_form')
checker_form = CheckUserIPsFormView.as_view()