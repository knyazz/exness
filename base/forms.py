#coding: utf-8
from __future__ import unicode_literals

from django import forms
from django.contrib.auth import get_user_model
User = get_user_model()

class CheckUserIPsForm(forms.Form):
    user1 = forms.ModelChoiceField(queryset=User.objects.all())
    user2 = forms.ModelChoiceField(queryset=User.objects.all())